import 'bootstrap/dist/css/bootstrap.css'
// import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap';
import ko from 'knockout';
import "./assets/main.scss"

// Here's my data model
var viewModel = function() {
    var self = this;
    this.tags = ko.observable('');
    this.editTags = ko.observable('');
    self.tempNum = ko.observableArray();
    self.showModal= ko.observable('')
    self.bodyModal= ko.observable('')
    //init data
    self.tempNum(localStorage.getItem('tags') ? JSON.parse(localStorage.getItem('tags')) : []);

    //Add Tags
    self.formSubmit = function(){
        if(self.tags()){
            var num = checknum(self.tags(), 'add');
            localStorage.setItem('tags',ko.toJSON(num));
            self.tags(null)
        }

    }

    //Remove Tags
    self.removeTags = function(index) {
        self.tempNum.remove(index)
        localStorage.setItem('tags',ko.toJSON(self.tempNum));
    }

    //Open Modal
    self.editTagsHandler = function () {
        var arr = [];
        self.showModal('showModal')
        self.bodyModal('open-modal')
        self.tempNum(localStorage.getItem('tags') ? JSON.parse(localStorage.getItem('tags')) : []);
        ko.utils.arrayForEach(self.tempNum(), function(tags) {
            arr.push(tags.value)
        });

        self.editTags(arr.join(','));
    }

    //Edit Tags
    self.editSubmit = function () {
        var num = checknum(self.editTags(), 'edit');
        localStorage.setItem('tags',ko.toJSON(num));
        self.tags(null);
        self.showModal('')
        self.bodyModal('')
    }

    // Close Modal when user click on overlay and close btn
    self.closeModal = function () {
        self.showModal('')
        self.bodyModal('')
    }

    //Check numbers and delimiters and push them into array
    function checknum(item, action){
        if(action == 'edit'){
            self.tempNum([]);
        }
        item =  item.split(/[,;\n]+/)
        for(var i = 0; i < item.length; i++){
            if(!isNaN(item[i]) && item[i] != ''){

                if(item[i] >= 0){
                    self.tempNum.push({value:item[i],color:"red"})
                }else{
                    self.tempNum.push({value:item[i],color:"blue"})
                }

            }
        }
        return self.tempNum
    }

};

ko.applyBindings(new viewModel());