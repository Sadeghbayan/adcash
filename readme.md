# Adcash Test

## installation guide
- Clone the `[git@gitlab.com:Sadeghbayan/adcash.git]` repository.
- Download npm "We use Node Package Manager (NPM) to download and manage packages in our application."
- Install project dependencies by running `npm install --save-dev`
- run `npm run dev` in project's root
- open your localhost with port `8081` on a browser
- to see the `Tests` just run `node_modules/.bin/cypress open` in project's root
