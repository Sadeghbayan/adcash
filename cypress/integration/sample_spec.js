beforeEach(() => {
    const clear = Cypress.LocalStorage.clear;
    Cypress.LocalStorage.clear = function (keys, ls, rs) {
        // do something with the keys here
        if (keys) {

            // return clear.apply(this, arguments)
        }
    }
})

describe('Test Tags', function() {
    it('Visits the Page', function() {
        cy.visit('http://localhost:8081')
    })
    it('Find Elements', function() {
        cy.get('.add_form')
        cy.get('#tags')
        cy.get('.custom_modal')
        cy.get('.overlay')
    })
    it('Insert Tags', function() {
        cy.get('.add_form #tags')
            .type('adasdas-;98,\n;-88,23-199m870,سیبیسبیسبیس')

    })
    it ('Check LocalStorage',  function () {
        cy.get('.add_form button').click().should(() => {
            expect(localStorage.getItem('tags'))
        })
    })

    it('Check Inserted Tags', function() {
        cy.get('pre ul li.tag__item span', function() {
            cy.contains('98')
            cy.contains('-88')
        })
    })
    it('Check Edit Tags', function() {
        cy.get('.tag__edit').click().get('.custom_modal').should('have.class' , 'showModal')
    })

    it('Check Edit Modal Form Tags', function() {
        cy.get('#tags_edit', function() {
            cy.contains('98')
            cy.contains('-88')
        })
    })
    it('Edit Action', function() {
        cy.get('.edit__form button').click().get('.showModal').should('not.exist');
    })
})